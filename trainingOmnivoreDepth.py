import numpy

from omnivore.models.omnivore_model import OmnivoreModel
import torch.nn as nn
import torch
from omnivision.models.swin_transformer import SwinTransformer3D
import torchvision.transforms as T
from dataloaderCSVDepthNoPreload import Data
from tqdm.auto import tqdm
from torch import optim
import torch.nn as nn
import pandas as pd

def get_all_heads(dim_in: int = 1024) -> nn.Module:
    heads = nn.ModuleDict(
        {
            "image": get_imagenet_head(dim_in),
            "rgbd": get_sunrgbd_head(dim_in),
            "video": get_kinetics_head(dim_in),
        }
    )
    return heads

classes = 12

def get_imagenet_head(dim_in: int = 1024) -> nn.Module:
    head = nn.Linear(in_features=dim_in, out_features=12, bias=True)
    return head


def get_sunrgbd_head(dim_in: int = 1024) -> nn.Module:
    head = nn.Linear(in_features=dim_in, out_features=12, bias=True)
    return head


def get_kinetics_head(dim_in: int = 1024, num_classes: int = classes) -> nn.Module:
    head = nn.Linear(in_features=dim_in, out_features=num_classes, bias=True)
    return nn.Sequential(nn.Dropout(p=0.5), head)

heads = get_all_heads()

trunk = SwinTransformer3D(
    pretrained2d=False,
    patch_size=(2, 4, 4),
    embed_dim=128,
    depths=[2, 2, 18, 2],
    num_heads=[4, 8, 16, 32],
    window_size=(16, 7, 7),
    drop_path_rate=0.3,  # TODO: set this based on the final models
    patch_norm=True,  # Make this the default value?
    depth_mode="rgbd",  # Make this the default value?
    )

trunk = SwinTransformer3D(
    pretrained=None,
    pretrained2d=False,
    pretrained3d=None,
    pretrained_model_key="base_model",
    patch_size=(4, 4, 4),
    embed_dim=128,
    depths=[2, 2, 18, 2],
    num_heads=[4, 8, 16, 32],
    window_size=(2, 7, 7),
    mlp_ratio=4.0,
    qkv_bias=True,
    qk_scale=None,
    drop_rate=0.0,
    attn_drop_rate=0.0,
    drop_path_rate=0.2,
    norm_layer=nn.LayerNorm,
    patch_norm=False,
    frozen_stages=-1,
    depth_mode="rgbd",
    depth_patch_embed_separate_params=True,
    )

device = "cuda" if torch.cuda.is_available() else "cpu"
print(device)

trainsetcsv = "trainset.csv"
trainset = pd.read_csv(trainsetcsv, sep=' ')
valsetcsv = "valset.csv"
valset = pd.read_csv(valsetcsv, sep=' ')


type_path_depth = r'\cam4\depth\*.png'
type_path_rgb256 = r'\cam4\rgb256\*.jpg'
main_path = r'D:\Users\claudaff'



train_img = Data(main_path,type_path_rgb256,type_path_depth,trainset, False)
trainloader = torch.utils.data.DataLoader(train_img, batch_size=2, shuffle=True, num_workers=0, pin_memory=True)
n_train = len(trainloader)

print("Train set loaded")

val_img = Data(main_path,type_path_rgb256,type_path_depth,valset, False)
valloader = torch.utils.data.DataLoader(val_img, batch_size=1, shuffle=False, num_workers=0, pin_memory=True)
n_val = len(valloader)

print("Validation set loaded")


model = OmnivoreModel(trunk,heads)
print(model)
model = model.to(device)
model = model.train()

# Loss function
ce_loss = nn.CrossEntropyLoss(reduction='mean')

base_lr = 1e-3
weight_decay = 0.009
#optimizer = torch.optim.Adam(model.parameters(), lr=base_lr, weight_decay=weight_decay)
optimizer = torch.optim.SGD(model.parameters(),lr=base_lr)
#optimizer = torch.optim.RMSprop(model.parameters(),lr=base_lr)
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5, patience=3, verbose=True)


torch.cuda.empty_cache()
print(torch.cuda.memory_summary(device=None, abbreviated=False))


epochs = 200
loss_data = []

largestAccuracy = 0
batch_size = 1

# Loop over the dataset multiple times
for epoch in range(epochs):
    train_loss = 0
    model.train()
    with tqdm(total=int(n_train * batch_size) - 1, desc=f'Epoch {epoch + 1}/{epochs}', unit='video',
              bar_format='{desc:<5.5}{percentage:3.0f}%|{bar:10}{r_bar}') as pbar:
        for i, data in enumerate(trainloader):
            # get the inputs; data is a list of [inputs, labels]

            inputs = data["video"]
            labels = data["action"]

            #print(inputs.shape)

            inputs, labels = inputs.cuda(), labels.cuda()

            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs, input_type="video")

            loss = ce_loss(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            train_loss += loss.item()
            #loss_data.append(loss.item())

            # Update the pbar
            pbar.update(batch_size)

            # Add loss (batch) value to tqdm
            pbar.set_postfix(**{'ce_loss': loss.item()})

    loss_data.append(train_loss)

    model.eval()
    total = 0
    correct = 0
    for i, data in enumerate(valloader):
        with torch.no_grad():

            inputs = data["video"]
            labels = data["action"]

            inputs, labels = inputs.cuda(), labels.cuda()

            outputs = model(inputs, input_type="video")
            pred_classes = outputs.topk(k=1).indices

            total += 1

            if (labels in pred_classes):
                correct += 1

    accuracy = correct/total
    print("Accuracy: ", correct, total, accuracy)

    if largestAccuracy < accuracy:

        print(f'Accuracy Increased({largestAccuracy:.6f}--->{accuracy:.6f}) \t Saving The Model')
        largestAccuracy = accuracy
        # Saving State Dict
        torch.save(model.state_dict(), 'depth_train.torch')
        numpy.save("trainingLoss_depth", loss_data)


print('Finished Training')
