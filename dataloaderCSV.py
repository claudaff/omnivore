import numpy as np
from torch.utils import data
import glob
import cv2
import torchvision.transforms as T

from pytorchvideo.transforms import (
    ApplyTransformToKey,
    ShortSideScale,
    UniformTemporalSubsample,

)
from torchvision.transforms._transforms_video import NormalizeVideo
import torch
from torchvision.transforms._transforms_video import CenterCropVideo
import gc
import pandas as pd
import time
start_time = time.time()

trainsetcsv = "trainset.csv"
trainset = pd.read_csv(trainsetcsv, sep=' ')

type_path_rgb256 = r'\cam4\rgb256\*.jpg'
main_path = r'D:\Users\claudaff'
b = '\\'

frames = 6


video_transform = T.Compose(
    [
        UniformTemporalSubsample(frames),
        T.Lambda(lambda x: x / 255.0),
        CenterCropVideo(224),
        NormalizeVideo(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ]

)

def ImagesToVideoList(main_path, type_path, csv):

    path = np.array(csv.loc[:, "path"])
    labels = np.array(csv.loc[:, "action_label"])
    startframe = np.array(csv.loc[:, "start_act"])
    endframe = np.array(csv.loc[:, "end_act"])

    video_list = []

    for i in range(len(path)):

        image_list = [] # fill with frames of same action

        p = str(main_path + b + path[i] + type_path)
        start = startframe[i]
        end = endframe[i]

        ind = 0
        for filename in sorted(glob.glob(p)):

            if(ind in range(start,end + 1)):

                im = cv2.imread(fr"{filename}")
                image_list.append(im)
            ind += 1

        video = np.array(image_list)
        video = torch.from_numpy(video)
        video = np.transpose(video, (3, 0, 1, 2))
        video = video_transform(video)

        #print(video.shape) # torch.Size([3, 6, 244, 244])

        video_list.append(video)

        #print(len(video_list))

        """""

        print(video.shape)
        v = np.transpose(video, (1, 2, 3, 0))

        plt.imshow(v[0])
        plt.show()
        plt.imshow(v[1])
        plt.show()
        plt.imshow(v[2])
        plt.show()
        plt.imshow(v[3])
        plt.show()
        plt.imshow(v[4])
        plt.show()
        plt.imshow(v[5])
        plt.show()
        
        """""


    return video_list, labels


class Data(data.Dataset):
    def __init__(self, main_path, type_path, csv):

        self.main_path = main_path
        self.type_path = type_path
        self.csv = csv

        self.videos , self.labels = ImagesToVideoList(main_path,type_path,csv)


    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):


        videoFrames = self.videos[index]


        action = self.labels[index]
        action = torch.tensor(action)

        output = {

            "video": videoFrames,
            "action": action,
        }

        return output

"""""

train_img = Data(main_path,type_path_rgb256,trainset)
trainloader = torch.utils.data.DataLoader(train_img, batch_size=1, shuffle=True, num_workers=0, pin_memory=True)

dataiter = iter(trainloader)
out = next(dataiter)
print("--- %s seconds ---" % (time.time() - start_time))

o = out["video"]
q = out["action"]

print(q)
print(o.shape)

v = o[0]
print(v.shape)

v = np.transpose(v, (1,2,3,0))



plt.imshow(v[0])
plt.show()
plt.imshow(v[1])
plt.show()
plt.imshow(v[2])
plt.show()
plt.imshow(v[3])
plt.show()
plt.imshow(v[4])
plt.show()
plt.imshow(v[5])
plt.show()

"""""