import matplotlib.pyplot as plt
import numpy as np
from torch.utils import data
import glob
import cv2
import torchvision.transforms as T

from pytorchvideo.transforms import (
    ApplyTransformToKey,
    ShortSideScale,
    UniformTemporalSubsample,

)
from torchvision.transforms._transforms_video import NormalizeVideo
import torch
from torchvision.transforms._transforms_video import CenterCropVideo
import gc
import pandas as pd
import time
import math
from omnivore.transforms import SpatialCrop, TemporalCrop, DepthNorm

start_time = time.time()

trainsetcsv = "trainset.csv"
trainset = pd.read_csv(trainsetcsv, sep=' ')

type_path_depth = r'\cam4\depth\*.png'
type_path_rgb256 = r'\cam4\rgb256\*.jpg'
main_path = r'D:\Users\claudaff'
b = '\\'

frames = 8

video_transform = T.Compose(
    [
        UniformTemporalSubsample(frames),
        T.Lambda(lambda x: x / 255.0),
        CenterCropVideo(224),
        NormalizeVideo(mean=[0.485, 0.456, 0.406, 0.0418],
            std=[0.229, 0.224, 0.225, 0.0295]),  # before not active
    ]

)


def ImagesToVideoList(main_path, type_path1, type_path2, csv, verbs):
    path = np.array(csv.loc[:, "path"])
    labels = np.array(csv.loc[:, "action_label"])

    if (verbs):

        grab = [1, 2, 3, 4, 5, 6, 7, 8]
        place = [9, 10, 11, 12, 13, 14, 15, 16]
        open = [17, 18, 19]
        close = [20, 21, 22]
        pour = 23
        takeout = [24, 25, 26, 27]
        put = [28, 29, 30]
        apply = [31, 32]
        read = [33, 34]
        spray = 35
        squeeze = 36

        for i in range(len(labels)):

            act = labels[i]

            if (act in grab):

                labels[i] = 1

            elif (act in place):

                labels[i] = 2

            elif (act in open):

                labels[i] = 3

            elif (act in close):

                labels[i] = 4

            elif (act == pour):

                labels[i] = 5

            elif (act in takeout):

                labels[i] = 6

            elif (act in put):

                labels[i] = 7

            elif (act in apply):

                labels[i] = 8

            elif (act in read):

                labels[i] = 9

            elif (act == spray):

                labels[i] = 10

            elif (act == squeeze):

                labels[i] = 11

            else:

                print("?")

    startframe = np.array(csv.loc[:, "start_act"])
    endframe = np.array(csv.loc[:, "end_act"])

    video_list_rgb = []
    video_list_depth = []

    for i in range(len(path)):

        image_list_rgb = []  # fill with frames of same action
        image_list_depth = []

        p1 = str(main_path + b + path[i] + type_path1)
        p2 = str(main_path + b + path[i] + type_path2)
        start = startframe[i]
        end = endframe[i]

        ind = 0
        for filename in sorted(glob.glob(p1)):



            if (ind in range(start, end + 1)):

                image_list_rgb.append(filename)



            ind += 1

        l = int(math.floor(len(image_list_rgb) / frames))
        image_list_rgb = image_list_rgb[::l]

        ind = 0
        for filename in sorted(glob.glob(p2)):

            if (ind in range(start, end + 1)):
                image_list_depth.append(filename)

            ind += 1

        l = int(math.floor(len(image_list_depth) / frames))
        image_list_depth = image_list_depth[::l]


        video_list_rgb.append(image_list_rgb)
        video_list_depth.append(image_list_depth)

    return video_list_rgb,video_list_depth, labels


class Data(data.Dataset):
    def __init__(self, main_path, type_path1, type_path2,csv, verbs):
        self.main_path = main_path
        self.type_path1 = type_path1
        self.type_path2 = type_path2
        self.csv = csv
        self.verbs = verbs

        self.videos_rgb,self.videos_depth, self.labels = ImagesToVideoList(main_path, type_path1,type_path2, csv, verbs)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        videoFramesRGB = self.videos_rgb[index]

        videoFramesListRGB = []

        for imagefile in videoFramesRGB:

            img = cv2.imread(fr"{imagefile}")
            videoFramesListRGB.append(img)


        videoFramesD = self.videos_depth[index]

        videoFramesListD = []

        for imagefile in videoFramesD:

            img = cv2.imread(fr"{imagefile}",0)
            img = cv2.resize(img, (0, 0), fx=0.3555, fy=0.3555)
            depth = img.reshape((256,455,1))
            videoFramesListD.append(depth)

        if(len(videoFramesListRGB) != len(videoFramesListD)):

            print("Lists need to be same size")
            exit()

        videoFramesListRGBD = []

        for i in range(len(videoFramesListRGB)):

            depth = videoFramesListD[i]
            rgb = videoFramesListRGB[i]

            rgbd = np.dstack((rgb, depth))

            videoFramesListRGBD.append(rgbd)

        video = np.array(videoFramesListRGBD)
        video = torch.from_numpy(video)
        video = np.transpose(video, (3, 0, 1, 2))
        video = video_transform(video)



        action = self.labels[index]
        action = torch.tensor(action)

        output = {

            "video": video,
            "action": action,
        }

        return output



train_img = Data(main_path,type_path_rgb256,type_path_depth,trainset, False)
trainloader = torch.utils.data.DataLoader(train_img, batch_size=1, shuffle=True, num_workers=0, pin_memory=True)
for i, data in enumerate(trainloader):

    o = data["video"][0]
    q = data["action"]
    print(q)
    v = np.transpose(o, (1, 2, 3, 0))
    pic = v[0]
    plt.imshow(pic[:,:,3])
    plt.show()
    plt.imshow(pic[:,:,:3])
    plt.show()

    print(v.shape)



