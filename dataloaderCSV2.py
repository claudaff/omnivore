import matplotlib.pyplot as plt
import numpy as np
from torch.utils import data
import glob
import cv2
import torchvision.transforms as T

from pytorchvideo.transforms import (
    ApplyTransformToKey,
    ShortSideScale,
    UniformTemporalSubsample,

)
from torchvision.transforms._transforms_video import NormalizeVideo
import torch
from torchvision.transforms._transforms_video import CenterCropVideo
import gc
import pandas as pd
import time
import math

start_time = time.time()

trainsetcsv = "trainset.csv"
trainset = pd.read_csv(trainsetcsv, sep=' ')

type_path_rgb256 = r'\cam4\rgb256\*.jpg'
main_path = r'D:\Users\claudaff'
b = '\\'

frames = 4

video_transform = T.Compose(
    [
        UniformTemporalSubsample(frames),
        T.Lambda(lambda x: x / 255.0),
        CenterCropVideo(224),
        NormalizeVideo(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]), # before not active
    ]

)


def ImagesToVideoList(main_path, type_path, csv, verbs):
    path = np.array(csv.loc[:, "path"])
    labels = np.array(csv.loc[:, "action_label"])

    if (verbs):

        grab = [1, 2, 3, 4, 5, 6, 7, 8]
        place = [9, 10, 11, 12, 13, 14, 15, 16]
        open = [17, 18, 19]
        close = [20, 21, 22]
        pour = 23
        takeout = [24, 25, 26, 27]
        put = [28, 29, 30]
        apply = [31, 32]
        read = [33, 34]
        spray = 35
        squeeze = 36

        for i in range(len(labels)):

            act = labels[i]

            if (act in grab):

                labels[i] = 1

            elif (act in place):

                labels[i] = 2

            elif (act in open):

                labels[i] = 3

            elif (act in close):

                labels[i] = 4

            elif (act == pour):

                labels[i] = 5

            elif (act in takeout):

                labels[i] = 6

            elif (act in put):

                labels[i] = 7

            elif (act in apply):

                labels[i] = 8

            elif (act in read):

                labels[i] = 9

            elif (act == spray):

                labels[i] = 10

            elif (act == squeeze):

                labels[i] = 11

            else:

                print("?")

    startframe = np.array(csv.loc[:, "start_act"])
    endframe = np.array(csv.loc[:, "end_act"])

    video_list = []

    for i in range(len(path)):

        image_list = []  # fill with frames of same action

        p = str(main_path + b + path[i] + type_path)
        start = startframe[i]
        end = endframe[i]

        ind = 0
        for filename in sorted(glob.glob(p)):



            if (ind in range(start, end + 1)):
                

                image_list.append(filename)



            ind += 1

        l = int(math.floor(len(image_list) / frames))
        image_list = image_list[::l]


        video_list.append(image_list)

    return video_list, labels


class Data(data.Dataset):
    def __init__(self, main_path, type_path, csv, verbs):
        self.main_path = main_path
        self.type_path = type_path
        self.csv = csv
        self.verbs = verbs

        self.videos, self.labels = ImagesToVideoList(main_path, type_path, csv, verbs)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        videoFrames = self.videos[index]

        videoFramesList = []

        for imagefile in videoFrames:


            img = cv2.imread(fr"{imagefile}")
            #print(img.shape)

            videoFramesList.append(img)

        video = np.array(videoFramesList)
        video = torch.from_numpy(video)
        video = np.transpose(video, (3, 0, 1, 2))
        video = video_transform(video)



        action = self.labels[index]
        action = torch.tensor(action)

        output = {

            "video": video,
            "action": action,
        }

        return output

"""""

train_img = Data(main_path,type_path_rgb256,trainset, False)
trainloader = torch.utils.data.DataLoader(train_img, batch_size=1, shuffle=True, num_workers=0, pin_memory=True)

for i, data in enumerate(trainloader):

    o = data["video"][0]
    v = np.transpose(o, (1, 2, 3, 0))

    plt.imshow(v[0])
    plt.show()
    plt.imshow(v[1])
    plt.show()
    plt.imshow(v[2])
    plt.show()
    plt.imshow(v[3])
    plt.show()
    


dataiter = iter(trainloader)
out = next(dataiter)
print("--- %s seconds ---" % (time.time() - start_time))
"""""

